﻿namespace Launcher
{
    public enum LauncherServerPacketTypes : byte
    {
        FileList = 1,
        FilePart,
        FileComplete

    }
}