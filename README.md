# README #

The Wizards Engine is licensed under LGPL v3 and mainly developed by Michiel Huygen.

### WARNING ###
This is the old version of the repository (with some large files). The repository was pruned from big
files and moved to a new location on https://github.com/mhgamework/the-wizards-engine

### How do I get set up? ###

See [Installation guidelines](http://wiki.thewizards.be/doku.php?id=developmentinstallation) on how to setup your PC for development with The Wizards Engine.

Read "_How to use.txt" for some extra info. (TODO merge with install guidelines and maybe add an installation guide to git)

### Who do I talk to? ###

Contact me at mhgamework at gmail dot com

### Contributors ###

Michiel Huygen: Lead developer, engine development

Simon Chuptys: Artist, development

Bart Philippe: Development (legacy tree engine, particle system, imposter engine)

Japser Hilven: Gameplay code