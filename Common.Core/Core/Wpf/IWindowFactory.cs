﻿using System.Windows;

namespace MHGameWork.TheWizards.Wpf
{
    public interface IWindowFactory
    {
        Window CreateWindow();
    }
}
