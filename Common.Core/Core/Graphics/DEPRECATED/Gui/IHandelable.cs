using System;
using System.Collections.Generic;
using System.Text;

namespace MHGameWork.TheWizards.ServerClient.Gui
{
    public interface IHandelable
    {
        bool Handled
        {
            get;
            set;
        }

    }
}
