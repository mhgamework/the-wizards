﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MHGameWork.TheWizards.Common.GeoMipMap
{
    public enum TerrainBlockEdge
    {
        West,
        East,
        North,
        South
    }
}
