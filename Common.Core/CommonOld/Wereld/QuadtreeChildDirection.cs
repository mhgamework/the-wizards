using System;
using System.Collections.Generic;
using System.Text;

namespace MHGameWork.TheWizards.Common.Wereld
{
    public enum QuadtreeChildDirection
    {
        TopLeft = 0,
        TopRight = 1,
        BottomLeft = 2,
        BottomRight = 3
    }
}
