<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DesignFileManager
    Inherits DesignMainPanel

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtHash = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.txtEnabled = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.txtType = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.txtVersie = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.txtLocalFile = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.txtRelativePath = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.txtDescription = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.lblID = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel9 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel8 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel7 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel6 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel5 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel4 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.txtFileName = New MHGameWork.TheWizards.Common.DesignTextBox2D
        Me.DesignLabel3 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel2 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.DesignLabel1 = New MHGameWork.TheWizards.Common.DesignLabel
        Me.knpUpdateGameFiles = New MHGameWork.TheWizards.Common.DesignKnop001
        Me.lstFiles = New MHGameWork.TheWizards.Common.DesignListBox2D
        Me.knpAddFile = New MHGameWork.TheWizards.Common.DesignKnop001
        Me.knpHoofdmenu = New MHGameWork.TheWizards.Common.DesignKnop001
        Me.SuspendLayout()
        '
        'txtHash
        '
        Me.txtHash.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtHash.Location = New System.Drawing.Point(366, 415)
        Me.txtHash.Name = "txtHash"
        Me.txtHash.Size = New System.Drawing.Size(334, 36)
        Me.txtHash.TabIndex = 19
        Me.txtHash.TextBoxText = ""
        '
        'txtEnabled
        '
        Me.txtEnabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtEnabled.Location = New System.Drawing.Point(366, 373)
        Me.txtEnabled.Name = "txtEnabled"
        Me.txtEnabled.Size = New System.Drawing.Size(334, 36)
        Me.txtEnabled.TabIndex = 18
        Me.txtEnabled.TextBoxText = ""
        '
        'txtType
        '
        Me.txtType.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtType.Location = New System.Drawing.Point(366, 331)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(334, 36)
        Me.txtType.TabIndex = 17
        Me.txtType.TextBoxText = ""
        '
        'txtVersie
        '
        Me.txtVersie.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtVersie.Location = New System.Drawing.Point(366, 289)
        Me.txtVersie.Name = "txtVersie"
        Me.txtVersie.Size = New System.Drawing.Size(334, 36)
        Me.txtVersie.TabIndex = 16
        Me.txtVersie.TextBoxText = ""
        '
        'txtLocalFile
        '
        Me.txtLocalFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtLocalFile.Location = New System.Drawing.Point(366, 247)
        Me.txtLocalFile.Name = "txtLocalFile"
        Me.txtLocalFile.Size = New System.Drawing.Size(334, 36)
        Me.txtLocalFile.TabIndex = 15
        Me.txtLocalFile.TextBoxText = ""
        '
        'txtRelativePath
        '
        Me.txtRelativePath.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtRelativePath.Location = New System.Drawing.Point(366, 205)
        Me.txtRelativePath.Name = "txtRelativePath"
        Me.txtRelativePath.Size = New System.Drawing.Size(334, 36)
        Me.txtRelativePath.TabIndex = 14
        Me.txtRelativePath.TextBoxText = ""
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDescription.Location = New System.Drawing.Point(366, 163)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(334, 36)
        Me.txtDescription.TabIndex = 13
        Me.txtDescription.TextBoxText = ""
        '
        'lblID
        '
        Me.lblID.BackColor = System.Drawing.Color.Green
        Me.lblID.LabelText = "ID"
        Me.lblID.Location = New System.Drawing.Point(366, 79)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(108, 36)
        Me.lblID.TabIndex = 12
        Me.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DesignLabel9
        '
        Me.DesignLabel9.LabelText = "LocalFile"
        Me.DesignLabel9.Location = New System.Drawing.Point(252, 247)
        Me.DesignLabel9.Name = "DesignLabel9"
        Me.DesignLabel9.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel9.TabIndex = 11
        Me.DesignLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel8
        '
        Me.DesignLabel8.LabelText = "Hash"
        Me.DesignLabel8.Location = New System.Drawing.Point(252, 415)
        Me.DesignLabel8.Name = "DesignLabel8"
        Me.DesignLabel8.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel8.TabIndex = 10
        Me.DesignLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel7
        '
        Me.DesignLabel7.LabelText = "Type"
        Me.DesignLabel7.Location = New System.Drawing.Point(252, 331)
        Me.DesignLabel7.Name = "DesignLabel7"
        Me.DesignLabel7.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel7.TabIndex = 9
        Me.DesignLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel6
        '
        Me.DesignLabel6.LabelText = "RelativePath"
        Me.DesignLabel6.Location = New System.Drawing.Point(252, 205)
        Me.DesignLabel6.Name = "DesignLabel6"
        Me.DesignLabel6.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel6.TabIndex = 8
        Me.DesignLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel5
        '
        Me.DesignLabel5.LabelText = "Description"
        Me.DesignLabel5.Location = New System.Drawing.Point(252, 163)
        Me.DesignLabel5.Name = "DesignLabel5"
        Me.DesignLabel5.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel5.TabIndex = 7
        Me.DesignLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel4
        '
        Me.DesignLabel4.LabelText = "ID"
        Me.DesignLabel4.Location = New System.Drawing.Point(252, 79)
        Me.DesignLabel4.Name = "DesignLabel4"
        Me.DesignLabel4.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel4.TabIndex = 6
        Me.DesignLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFileName
        '
        Me.txtFileName.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtFileName.Location = New System.Drawing.Point(366, 121)
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.Size = New System.Drawing.Size(334, 36)
        Me.txtFileName.TabIndex = 5
        Me.txtFileName.TextBoxText = ""
        '
        'DesignLabel3
        '
        Me.DesignLabel3.LabelText = "Enabled"
        Me.DesignLabel3.Location = New System.Drawing.Point(252, 373)
        Me.DesignLabel3.Name = "DesignLabel3"
        Me.DesignLabel3.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel3.TabIndex = 4
        Me.DesignLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel2
        '
        Me.DesignLabel2.LabelText = "Versie"
        Me.DesignLabel2.Location = New System.Drawing.Point(252, 289)
        Me.DesignLabel2.Name = "DesignLabel2"
        Me.DesignLabel2.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel2.TabIndex = 3
        Me.DesignLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DesignLabel1
        '
        Me.DesignLabel1.LabelText = "Filename"
        Me.DesignLabel1.Location = New System.Drawing.Point(252, 121)
        Me.DesignLabel1.Name = "DesignLabel1"
        Me.DesignLabel1.Size = New System.Drawing.Size(108, 36)
        Me.DesignLabel1.TabIndex = 2
        Me.DesignLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'knpUpdateGameFiles
        '
        Me.knpUpdateGameFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.knpUpdateGameFiles.KnopText = "Update Client Files List"
        Me.knpUpdateGameFiles.Location = New System.Drawing.Point(31, 19)
        Me.knpUpdateGameFiles.Name = "knpUpdateGameFiles"
        Me.knpUpdateGameFiles.Size = New System.Drawing.Size(198, 35)
        Me.knpUpdateGameFiles.TabIndex = 1
        '
        'lstFiles
        '
        Me.lstFiles.BackColor = System.Drawing.Color.Red
        Me.lstFiles.ItemHeight = 30
        Me.lstFiles.Location = New System.Drawing.Point(31, 69)
        Me.lstFiles.Name = "lstFiles"
        Me.lstFiles.Size = New System.Drawing.Size(198, 548)
        Me.lstFiles.TabIndex = 0
        '
        'knpAddFile
        '
        Me.knpAddFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.knpAddFile.KnopText = "Add new Client File"
        Me.knpAddFile.Location = New System.Drawing.Point(31, 630)
        Me.knpAddFile.Name = "knpAddFile"
        Me.knpAddFile.Size = New System.Drawing.Size(198, 35)
        Me.knpAddFile.TabIndex = 20
        '
        'knpHoofdmenu
        '
        Me.knpHoofdmenu.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.knpHoofdmenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.knpHoofdmenu.KnopText = "Terug naar Hoofdmenu"
        Me.knpHoofdmenu.Location = New System.Drawing.Point(583, 630)
        Me.knpHoofdmenu.Name = "knpHoofdmenu"
        Me.knpHoofdmenu.Size = New System.Drawing.Size(203, 35)
        Me.knpHoofdmenu.TabIndex = 21
        '
        'DesignFileManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.Controls.Add(Me.knpHoofdmenu)
        Me.Controls.Add(Me.knpAddFile)
        Me.Controls.Add(Me.txtHash)
        Me.Controls.Add(Me.txtEnabled)
        Me.Controls.Add(Me.txtType)
        Me.Controls.Add(Me.txtVersie)
        Me.Controls.Add(Me.txtLocalFile)
        Me.Controls.Add(Me.txtRelativePath)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.DesignLabel9)
        Me.Controls.Add(Me.DesignLabel8)
        Me.Controls.Add(Me.DesignLabel7)
        Me.Controls.Add(Me.DesignLabel6)
        Me.Controls.Add(Me.DesignLabel5)
        Me.Controls.Add(Me.DesignLabel4)
        Me.Controls.Add(Me.txtFileName)
        Me.Controls.Add(Me.DesignLabel3)
        Me.Controls.Add(Me.DesignLabel2)
        Me.Controls.Add(Me.DesignLabel1)
        Me.Controls.Add(Me.knpUpdateGameFiles)
        Me.Controls.Add(Me.lstFiles)
        Me.Name = "DesignFileManager"
        Me.Size = New System.Drawing.Size(824, 697)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstFiles As Common.DesignListBox2D
    Friend WithEvents knpUpdateGameFiles As Common.DesignKnop001
    Friend WithEvents DesignLabel1 As Common.DesignLabel
    Friend WithEvents DesignLabel2 As Common.DesignLabel
    Friend WithEvents DesignLabel3 As Common.DesignLabel
    Friend WithEvents txtFileName As Common.DesignTextBox2D
    Friend WithEvents DesignLabel4 As Common.DesignLabel
    Friend WithEvents DesignLabel5 As Common.DesignLabel
    Friend WithEvents DesignLabel6 As Common.DesignLabel
    Friend WithEvents DesignLabel7 As Common.DesignLabel
    Friend WithEvents DesignLabel8 As Common.DesignLabel
    Friend WithEvents DesignLabel9 As Common.DesignLabel
    Friend WithEvents lblID As Common.DesignLabel
    Friend WithEvents txtDescription As Common.DesignTextBox2D
    Friend WithEvents txtRelativePath As Common.DesignTextBox2D
    Friend WithEvents txtLocalFile As Common.DesignTextBox2D
    Friend WithEvents txtVersie As Common.DesignTextBox2D
    Friend WithEvents txtType As Common.DesignTextBox2D
    Friend WithEvents txtEnabled As Common.DesignTextBox2D
    Friend WithEvents txtHash As Common.DesignTextBox2D
    Friend WithEvents knpAddFile As Common.DesignKnop001
    Friend WithEvents knpHoofdmenu As MHGameWork.TheWizards.Common.DesignKnop001


End Class
