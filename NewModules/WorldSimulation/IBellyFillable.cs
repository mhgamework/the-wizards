﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MHGameWork.TheWizards.WorldSimulation
{
    public interface IBellyFillable
    {
        float FoodLevel { get; set; }
    }
}
